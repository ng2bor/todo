import { Users } from './users';
import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user = {}
  constructor() {

    this.user = {
      'email': 'mi@miff.me',
      'password': '12345678',
      'fname': 'Milan',
      'avatar': 'https://en.gravatar.com/userimage/10744355/5d363288cd901dcbd122d6e01bf7a439.jpeg'
    }
  }
  
  tasks = []
  task = ''
  show = {
    completed: true,
    notcompleted: true
  }

  doToggle(i) {
    this.tasks[i].isComplited = !this.tasks[i].isComplited 
  }

  doAdd(e) {
    if (e.key == "Enter" && this.task.length > 2) {

      var tempTask = {
        title: this.task,
        created: Date(),
        isComplited: false
      }

      this.tasks.push(tempTask)
      this.task = ''
      //console.log(this.tasks)
    }
  }

}
