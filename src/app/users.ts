export class Users {
    id: Number
    full_name: String
    email: String
    password: String
    avatar: String
    active: Boolean
}
